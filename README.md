# Trabibi
![pipeline status](https://gitlab.com/rolafium/trabibi/badges/master/pipeline.svg)
![coverage](https://gitlab.com/rolafium/trabibi/badges/master/coverage.svg)

Trabibi is a tracking and comparing app.

You can find one instance of trabibi at [https://trabibi.rolafium.com](https://trabibi.rolafium.com)

# Developer Data
The developer data is re-generated at each commit.

* [Python Documentation](https://trabibi.rolafium.com/public/docs/python)
* [JavaScript Documentation](https://trabibi.rolafium.com/public/docs/javascript)
* [Python Coverage](https://trabibi.rolafium.com/public/coverage/python)
* [JavaScript Coverage](https://trabibi.rolafium.com/public/coverage/javascript)
