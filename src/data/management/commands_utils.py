
class SubCommandResult:
    """Create a command result manager that handles output and exit codes"""

    def __init__(self, command, quiet=False):
        """
        Inits the subcommand result object
        :param command: django.core.management.base.BaseCommand
        :param quiet: bool - Whether we write to stdout
        :returns: SubCommandResult
        """
        self.command = command
        self.write = self.command.stdout.write
        self.style = self.command.style
        self.quiet = quiet

        self.stdout = None
        self.stderr = None
        self.success = None
        self.data = None

    def keep_output(self, stdout, stderr):
        """
        Keeps the output of a command
        :param stdout:
        :param stderr:
        :returns: self
        """

        self.stdout = stdout
        self.stderr = stderr
        self.autocheck()

        return self

    def autocheck(self):
        """Autochecks if the stderr is present (meaning the command failed)"""
        if self.stderr:
            self.success = False
        else:
            self.success = True

        return self

    def write_success(self, reason=None):
        """
        Writes a success string to stdout
        :param reason: str
        :returns: self
        """
        if self.quiet:
            return self

        if reason is None:
            reason = "Success"
        self.write(self.style.SUCCESS(reason))

        return self

    def write_failure(self, reason=None):
        """
        Writes a failure string to stdout
        :param reason: str
        :returns: self
        """
        if self.quiet:
            return self

        if reason is None:
            reason = "Unspecified error"
        self.write(self.style.ERROR(reason))

        return self

    def hard_fail(self, exit_code=1, reason=None):
        """
        Hard fails and then exits
        :param exit_code: Int
        :param reason: str
        """
        self.write_failure(reason)
        exit(exit_code)

