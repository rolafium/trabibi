from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from data.default_data import DefaultDataIterator

class Command(BaseCommand): # pragma: no cover
    """Creates the default data (if required)"""
    help = 'Creates the default data (if required)'

    def add_arguments(self, parser):
        """Adds the required data_path argument"""
        parser.add_argument('data_path', type=str)

    def handle(self, *args, **options):
        """ Iterates via the DefaultDataIterator"""
        iterator = DefaultDataIterator(settings.ENVIROMENT)
        data = iterator.open_json(options["data_path"])
        iterator.iterate(data)

        self.stdout.write(self.style.SUCCESS('Successfully imported default data'))
