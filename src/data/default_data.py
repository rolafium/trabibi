import json
import logging
from importlib import import_module
from django.conf import settings
from django.db import transaction
from django.db.utils import IntegrityError

logger = logging.getLogger("applog")

class DefaultDataIterator:
    """Iterates the data and creates objects if required"""

    def __init__(self, enviroment=""):
        """
        Creates a DefaultDataIterator object

        :param enviroment: (str) The enviroment in which we will operate
        :returns: DefaultDataIterator
        """
        self.environment = enviroment

    def open_json(self, path):
        """
        Reads a json files and returns its data

        :param path: (str) The path of the json file
        :returns: dict
        """
        logger.info("Opening json: {path}".format(path=path))
        with open(path, "r") as fp:
            return json.load(fp)

    def iterate(self, data):
        """
        Iterate a proper formatted data dict and
        creates files where necessary

        :param data: (dict)
        """
        objs = []
        models = {}

        def _get_model(path):
            """Imports the model from the specified path"""
            if not models.get(path, None):
                path_splits = path.split(".")
                module = import_module(".".join(path_splits[:-1]))
                models[path] = getattr(module, path_splits[-1])

            return models[path]

        def _get_func(model, func_path=None):
            """Returns the function that will be used to create the object"""
            if func_path is None:
                def get_or_create_wrapper(**kwargs):
                    obj, created = model.objects.get_or_create(**kwargs)
                    if not created:
                        raise IntegrityError
                    return obj
                return get_or_create_wrapper
            else:
                func_path_splits = func_path.split(".")[1:]
                func = model
                for split in func_path_splits:
                    func = getattr(func, split)
                return func

        def _import_item(item):
            """Imports the item if required"""
            model = _get_model(item["model_path"])
            create_func = _get_func(model, item.get("func", None))

            with transaction.atomic():
                try:
                    obj = create_func(**item["data"])
                    objs.append(obj)
                    logger.info("Created: {data}".format(data=item["data"]))
                except IntegrityError:
                    logger.info("Already exists: {data}".format(data=item["data"]))
                    pass


        app_environ = getattr(settings, "ENVIROMENT", "no-enviroment").lower()

        for item in data.get("base", []):
            _import_item(item)


        for item in data.get(app_environ, []):
            _import_item(item)

    def __str__(self):
        """
        Returns the str representation of the object

        :returns: str
        """
        return "Default Data Iterator"

