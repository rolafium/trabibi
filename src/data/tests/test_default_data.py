import os
import json
import tempfile
from django.contrib.auth.models import User
from django.test import TestCase, override_settings
from data.default_data import DefaultDataIterator

data = {
    "base": [
        {
            "model_path": "django.contrib.auth.models.User",
            "func": "User.objects.create_user",
            "data": {
                "username": "UNIT_TEST_default_data_user",
                "email": "default_data_user@rolafium.com",
                "password": "default_data_user_password",
                "is_staff": False,
                "is_superuser": False
            }
        },    
    ],
    "some-enviroment": [
        {
            "model_path": "django.contrib.auth.models.User",
            "func": "User.objects.create_user",
            "data": {
                "username": "UNIT_TEST_default_data_user2",
                "email": "default_data_user2@rolafium.com",
                "password": "default_data_user_password",
                "is_staff": False,
                "is_superuser": False
            }
        }
    ],
    "testing": [
        {
            "model_path": "django.contrib.auth.models.User",
            "data": {
                "username": "UNIT_TEST_default_data_user_2",
                "email": "default_data_user2@rolafium.com",
                "password": "default_data_user_password",
                "is_staff": False,
                "is_superuser": False,
            }
        }
    ]
}

class TestDefaultDataIterator(TestCase):

    def setUp(self):
        """Delete all the default_data_users"""
        User.objects.filter(username__startswith="UNIT_TEST_default_data_user").delete()
        self.iterator = DefaultDataIterator("testing")

    def test_open_json(self):
        json_content = json.dumps(data)
        file_name = ""

        with tempfile.NamedTemporaryFile(mode="w", delete=False) as fp:
            file_name = fp.name
            fp.write(json_content)
            fp.close

        reloaded_data = self.iterator.open_json(file_name)
        os.unlink(file_name)

        self.assertTrue(reloaded_data, data)

    def test_iterator_matching_enviroment(self):
        """Shoud import only "testing" data and only once"""
        self.iterator.iterate(data)

        username = data["base"][0]["data"]["username"]
        password = data["base"][0]["data"]["password"]

        username2 = data["testing"][0]["data"]["username"]
        password2 = data["testing"][0]["data"]["password"]

        username3 = data["some-enviroment"][0]["data"]["username"]

        user = User.objects.get(username=username)
        user2 = User.objects.get(username=username2)
        user3_exists = User.objects.filter(username=username3).exists()

        user_count_before = User.objects.all().count()

        # User data is correct
        self.assertEqual(user.username, username)
        self.assertTrue(user.password != password)

        # User2 has been created with the normal get_or_create func
        self.assertEqual(user2.username, username2)
        self.assertTrue(user2.password == password2)

        # User3 has not been created (not matching env)
        self.assertFalse(user3_exists)

        # Users have not been created again
        self.iterator.iterate(data)
        user_count_after = User.objects.all().count()
        self.assertEqual(user_count_before, user_count_after)

    def test_generic(self):
        """Generic tests for the rest of the methods"""
        self.assertEqual(str, type(self.iterator.__str__()))
