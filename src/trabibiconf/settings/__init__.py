import os
from importlib import import_module
from django.conf import settings, global_settings


class SettingsLoader:

    def __init__(self, settings_module_path=None):
        if settings_module_path is None:
            settings_module_path = os.environ.get(
                "DJANGO_SETTING_MODULE", 
                "trabibiconf.settings.local_settings"
            )
        setting_module = import_module(settings_module_path)

        self._read_settings(global_settings)
        self._read_settings(setting_module.TrabibiSettings)


    def _read_settings(self, settings):
        for element in dir(settings):
            if not element.startswith('__'):
                setting_name = element
                setting_value = getattr(settings, setting_name)
                setattr(self, setting_name, setting_value)

    @classmethod
    def load_settings(cls, settings_module_path=None):
        if not settings.configured:
            loader = cls(settings_module_path)
            settings.configure(loader)
            return loader
