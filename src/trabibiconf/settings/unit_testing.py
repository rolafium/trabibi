import os
from trabibiconf.settings.base import BaseSettings


class TrabibiSettings(BaseSettings):

    ENVIROMENT = "TESTING"

    DEBUG = False
    PRIVATE_KEY = 'simple-private-key'
    ALLOWED_HOSTS = ["*"]

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BaseSettings.PROJECT_DIR, 'testdb.sqlite3'),
        }
    }

    STATIC_URL = '/static/'
    MEDIA_URL = '/media/'

    AUTH_PASSWORD_VALIDATORS = {}

    BaseSettings.LOGGING["loggers"]["applog"]["handlers"] = ["console"]
    BaseSettings.LOGGING["loggers"]["applog"]["level"] = "WARN"

    REQUEST_COP_IP_REQUEST_MAX = 5
