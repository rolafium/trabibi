import os
from trabibiconf.version import TRABIBI_VERSION_STR

_BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
_PROJECT_DIR = os.path.dirname(_BASE_DIR)

_BUILD_NUMBER = "x"
_BUILD_NUMBER_FILE = os.path.join(os.path.dirname(_PROJECT_DIR), "build_number.txt")

try:
    with open(_BUILD_NUMBER_FILE, "r") as fp:
        _BUILD_NUMBER = fp.read()
except FileNotFoundError:
    pass

class BaseSettings:

    ENVIROMENT = "BASE"
    BASE_DIR = _BASE_DIR
    PROJECT_DIR = _PROJECT_DIR
    SECRET_KEY = ''
    DEBUG = False
    ALLOWED_HOSTS = []

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'rest_framework',
        'django_request_cop',
        'data',
        'trabibi',
    ]

    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ]

    ROOT_URLCONF = 'trabibiconf.urls'

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                    'trabibi.context_processors.global_application_state',
                ],
            },
        },
    ]

    WSGI_APPLICATION = 'trabibiconf.wsgi.application'

    DATABASES = {}

    AUTH_PASSWORD_VALIDATORS = [
        { 'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator', },
        { 'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator', },
        { 'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator', },
        { 'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator', },
    ]

    LANGUAGE_CODE = 'en-us'
    TIME_ZONE = 'UTC'
    USE_I18N = True
    USE_L10N = True
    USE_TZ = True

    STATIC_URL = ''
    MEDIA_URL = ''

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
            },
            'file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': os.path.join(_PROJECT_DIR, "app.log"),
            },
        },
        'loggers': {
            'applog': {
                'handlers': ['file'],
                'level': 'DEBUG',
                'propagate': True,
            },
        },
    }

    REST_FRAMEWORK = {
        'DEFAULT_AUTHENTICATION_CLASSES': (
            'rest_framework.authentication.SessionAuthentication',
            'rest_framework.authentication.BasicAuthentication',
        ),
    }

    VERSION = TRABIBI_VERSION_STR
    BUILD_NUMBER = _BUILD_NUMBER

    REQUEST_COP_IP_REQUEST_MAX = 2
