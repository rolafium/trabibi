import os
from django.core.wsgi import get_wsgi_application
from trabibiconf.settings import SettingsLoader

SettingsLoader.load_settings()
application = get_wsgi_application()
