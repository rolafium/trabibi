from distutils.version import StrictVersion

TRABIBI_VERSION_STR = "0.0.1"
TRABIBI_RELEASE_STR = "0.0.1"
TRABIBI_VERSION = StrictVersion(TRABIBI_VERSION_STR)
TRABIBI_RELEASE = StrictVersion(TRABIBI_VERSION_STR)
