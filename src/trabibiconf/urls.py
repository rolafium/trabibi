from django.contrib import admin
from django.conf.urls import include, url
from django.urls import path

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("django_request_cop.urls")),
    path("", include("trabibi.urls")),
]
