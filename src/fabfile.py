import os
from io import BytesIO, StringIO
from tempfile import TemporaryFile
from fabric.api import cd, lcd, local, run, env, get, put, prefix
from fabric.operations import prompt


env.user = os.environ.get("TRABIBI_USER")
env.hosts = [ os.environ.get("TRABIBI_HOST") ]

if os.environ.get("TRABIBI_USE_ENV_KEY", "0") == "1":
    env.key = os.environ.get("TRABIBI_PRIVATE_KEY")
else:
    env.key_filename = os.environ.get("TRABIBI_PRIVATE_KEY_PATH")


def get_project_root():
    return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def run_python_tests():
    local_src_folder = os.path.join(get_project_root(), "src")
    with lcd(local_src_folder):
        local("tox")

def run_javascript_tests():
    project_root = get_project_root()
    local_js_src_folder = os.path.join(project_root, "src/trabibi/static/trabibi/js")
    with lcd(local_js_src_folder):
        local("npm test")

    target_coverage_folder = os.path.join(project_root, "public/coverage/javascript")
    local("mkdir -p {target_folder}".format(target_folder=target_coverage_folder))
    local("cp -r {coverage_folder} {target_folder}".format(
        coverage_folder=os.path.join(local_js_src_folder, "build/coverage/*"),
        target_folder=target_coverage_folder,
    ))


def run_tests():
    run_python_tests()
    run_javascript_tests()

def create_docs():
    project_root = get_project_root()
    src_folder = os.path.join(project_root, "src")
    local_docs_folder = os.path.join(project_root, "docs")
    sphinx_folder = os.path.join(local_docs_folder, "python")

    # Make the python docs with sphinx
    with lcd(sphinx_folder):
        local("make html")

    # Make the javascript docs with jsdoc
    with lcd(src_folder):
        local("jsdoc -c ../docs/javascript/.jsdoc.json")

    def _clean_folder_and_move(origin, target):
        local("mkdir -p {target}".format(target=target))
        local("rm -rf {target}/*".format(target=target))
        local("mv {origin} {target}".format(
            origin=origin,
            target=target,
        ))

    with lcd(project_root):
        _clean_folder_and_move("./docs/python/build/html/*", "./public/docs/python")
        _clean_folder_and_move("./docs/javascript/build/*", "./public/docs/javascript")

def upload_public_folder():
    local_public_folder = os.path.join(get_project_root(), "public")
    remote_target_folder = os.environ.get("TRABIBI_ROOT_DIR")
    put(local_public_folder, remote_target_folder)

def set_build_number():
    build_file = os.path.join(os.environ.get("TRABIBI_ROOT_DIR"), "build_number.txt")
    current_build = 0

    try:
        file_bytes = BytesIO()
        get(build_file, file_bytes)
        content = int(file_bytes.getvalue().decode("utf-8"))
        current_build = content + 1
    except (ValueError, FileNotFoundError) as e:
        question = "Could not read {file}. Please specify a build number:".format(file=build_file)
        current_build = int(prompt(question))

    print("Setting build number: " + str(current_build))
    content = StringIO("{build}\n".format(build=current_build))
    put(content, build_file)

def push():
    run_tests()
    local("git push origin master")

def deploy():
    run_tests()
    create_docs()
    set_build_number()

    code_dir = os.environ.get("TRABIBI_CODE_DIR")
    with cd(code_dir):
        run("git pull origin master")

    src_dir = os.path.join(code_dir, "src")
    with cd(src_dir):
        env_path = os.environ.get("TRABIBI_VIRTUAL_ENV_PATH")
        with prefix("source {path}".format(path=env_path)):
            run("python manage.py buildstatics")

    upload_public_folder()


