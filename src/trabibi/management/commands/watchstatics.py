import os
import json
import time
import subprocess
from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand): # pragma: no cover
    help = 'Builds the static files'

    def _kill_process(self, process, process_name):
        message = '\rQuitting {process_name}.'.format(process_name=process_name)
        self.stdout.write(self.style.SUCCESS(message))
        process.kill()

    def handle(self, *args, **options):
        target_dir = os.path.join(settings.BASE_DIR, "trabibi/static/trabibi")

        os.chdir(os.path.join(target_dir, "js"))
        self.stdout.write(self.style.SUCCESS('Watching JavaScript files.'))
        webpack_process = subprocess.Popen(["webpack", "--watch"])

        os.chdir(os.path.join(target_dir, "scss"))
        self.stdout.write(self.style.SUCCESS("Watching Scss files."))
        sass_process = subprocess.Popen(["sass", "--watch", "src:build"])

        while 1:
            try:
                time.sleep(200)
            except KeyboardInterrupt:
                self._kill_process(webpack_process, "webpack")
                self._kill_process(sass_process, "sass")
                break;

        exit(0)

