import os
import json
import subprocess
from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from data.management.commands_utils import SubCommandResult


class Command(BaseCommand): # pragma: no cover
    help = 'Builds the static files'

    def set_quiet(self, quiet):
        """Sets the verbosity of the command"""
        self.quiet = quiet

    def _spawn_subcommand(self, stdout, stderr):
        """
        Spawns a subcommand object
        :param stdout: str
        :param stderr: str
        """
        if not hasattr(self, "quiet"):
            self.quiet = False
        res = SubCommandResult(self, quiet=self.quiet).keep_output(stdout, stderr)
        return res

    def _update_npm_dependencies(self, target_dir="."):
        """
        Updates the javascript dependencies
        :param target_dir: str - Location in which we need to execute the cmd
        :returns: SubCommandResult
        """
        os.chdir(target_dir)

        npm_process = subprocess.Popen(
            ["npm", "install", "--silent"],
            stdout=subprocess.PIPE,
        )
        res = self._spawn_subcommand(*npm_process.communicate())

        if res.success:
            res.write_success("Successfully updated npm dependencies")
        else:
            res.write_failure("Could not update npm dependencies")

        return res

    def _run_webpack(self, target_dir="."):
        """
        Runs webpack to build the bundle js file
        :param target_dir: str - Location in which we need to execute the cmd
        :returns: SubCommandResult
        """
        os.chdir(target_dir)

        webpack_path = os.path.join(target_dir, "node_modules/.bin/webpack")
        webpack_process = subprocess.Popen(
            [webpack_path, "--mode", "production", "--json"],
            stdout=subprocess.PIPE,
        )
        
        res = self._spawn_subcommand(*webpack_process.communicate())

        if not res.success:
            res.hard_fail(
                exit_code=1, 
                reason="Failed to execute the Webpack command: {stderr}".format(stderr=res.stderr)
            )

        try:
            res.stdout = res.stdout.decode()
        except AttributeError:
            pass

        result = json.loads(res.stdout)

        if len(result["errors"]):
            res.hard_fail(
                exit_code=1,
                reason="Error bulding JavaScript files:\n{errors}".format(errors=result["errors"])
            )

        res.write_success("Successfully builded JavaScript files.")

        return res

    def _run_sass(self, target_dir="."):
        """
        Runs webpack to build the bundle js file
        :param target_dir: str - Location in which we need to execute the cmd
        :returns: SubCommandResult
        """
        os.chdir(target_dir)

        sass_process = subprocess.Popen(
            ["sass", "--update", "--force", "--style", "compressed", "src:build"],
            stdout=subprocess.PIPE,
        )

        res = self._spawn_subcommand(*sass_process.communicate())

        if not res.success:
            res.hard_fail(
                exit_code=1,
                reason="Failed to execute the Sass command: {stderr}".format(stderr=res.stderr)
            )

        res.write_success("Successfully builded Scss files.")

        return res

    def _collect_statics(self):
        """
        Runs the collecstatic management command
        :returns: SubCommandResult
        """
        call_command(
            "collectstatic", 
            verbosity=0, 
            interactive=False, 
            ignore=["node_modules"],
        )

        res = self._spawn_subcommand("", "")

        res.write_success("Successfully collected static files.")

        return res


    def handle(self, *args, **options):
        target_dir = os.path.join(settings.BASE_DIR, "trabibi/static/trabibi")
        self._update_npm_dependencies(os.path.join(target_dir, "js"))
        self._run_webpack(os.path.join(target_dir, "js"))
        self._run_sass(os.path.join(target_dir, "scss"))
        self._collect_statics()
