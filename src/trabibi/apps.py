from django.apps import AppConfig


class TrabibiConfig(AppConfig): # pragma: no cover
    name = 'trabibi'
