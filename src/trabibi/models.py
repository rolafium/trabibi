from functools import reduce
from django.db import models
from django.utils import timezone
from trabibi.utils import generate_model_dexie_schema



class TrackingObject(models.Model):

    name = models.CharField(max_length=128)
    parent = models.ForeignKey("trabibi.TrackingObject", on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class TrackingDetails(models.Model):

    obj = models.ForeignKey("trabibi.TrackingObject", on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    value = models.TextField()

    def __str__(self):
        return "{obj} - {name}".format(obj=self.obj, name=self.name)


class TrackingUpdate(models.Model):

    obj = models.ForeignKey("trabibi.TrackingObject", on_delete=models.CASCADE)
    relative_time = models.DateTimeField(default=timezone.now)
    value = models.FloatField()
    notes = models.TextField(default="")

    @classmethod
    def to_frontend_schema(cls):
        return ",".join(generate_model_dexie_schema(cls))

    def __str__(self):
        return "{obj} - {value}".format(obj=self.obj, value=self.value)

class TrackingGraph(models.Model):

    OPERATION_CHOICES = (
        ("SUM", "Sum"),
        ("DIFF", "Difference"),
    )

    name = models.CharField(max_length=128)
    parent = models.ForeignKey("trabibi.TrackingObject", related_name="graph_parent", on_delete=models.CASCADE)
    tracked = models.ManyToManyField("trabibi.TrackingObject", related_name="graph_tracked")
    operation = models.CharField(max_length=16, choices=OPERATION_CHOICES)

    def operate(self):
        values = self.tracked.all().values_list("trackingupdate__value", flat=True)
        if self.operation == "SUM":
            _sum = lambda x, y: x + y
            return reduce(_sum, values)
        else:
            return 0

    def __str__(self):
        return "{parent_name} - {graph_name} - {operation}".format(
            parent_name=self.parent.name,
            graph_name=self.name,
            operation=self.operation,
        )
