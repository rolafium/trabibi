from django.contrib.auth.models import User
from rest_framework import serializers
from trabibi.models import TrackingObject, TrackingDetails, TrackingUpdate, TrackingGraph


class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(write_only=True)
    password = serializers.CharField(
        write_only=True, 
        style={ "input_type": "password" },
    )

    def validate(self, data):
        data["username"] = data["email"]
        return data


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(
        write_only=True, 
        style={ "input_type": "password" },
    )
    username = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "username",
            "email",
            "password",
        )

    def create(self, validated_data):
        """Creates a user and sets its password"""
        data = validated_data.copy()
        data["username"] = data["email"]
        return User.objects.create_user(**data)


class TrackingObjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = TrackingObject
        fields = (
            "id",
            "name",
            "parent",
            "user",
        )


class TrackingDetailsSerializer(serializers.ModelSerializer):

    class Meta:
        model = TrackingDetails
        fields = (
            "id",
            "obj",
            "name",
            "value",
        )


class TrackingUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = TrackingUpdate
        fields = (
            "id",
            "obj",
            "relative_time",
            "value",
            "notes",
        )

class TrackingGraphSerializer(serializers.ModelSerializer):

    result = serializers.CharField(max_length=128, source="operate", read_only=True)

    class Meta:
        model = TrackingGraph
        fields = (
            "id",
            "name",
            "parent",
            "tracked",
            "operation",
            "result",
        )




class TrackingObjectDetailsSerializer(TrackingObjectSerializer):

    details = TrackingDetailsSerializer(source="trackingdetails_set.all", many=True)
    graphs = TrackingGraphSerializer(source="graph_parent.all", many=True)

    class Meta:
        model = TrackingObject
        fields = TrackingObjectSerializer.Meta.fields + (
            "details",
            "graphs",
        )

