from django.contrib import admin
from trabibi.models import *


admin.site.register(TrackingObject)
admin.site.register(TrackingDetails)
admin.site.register(TrackingGraph)
admin.site.register(TrackingUpdate)
