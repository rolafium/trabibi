from django.conf import settings

def global_application_state(request):
    """
    Adds the application state to the request context
    :param request: WsgiRequest
    :returns: dict
    """
    return {
        "TRABIBI_VERSION": settings.VERSION,
        "TRABIBI_BUILD_NUMBER": settings.BUILD_NUMBER,
        "TRABIBI_FULL_VERSION": "{}.{}".format(settings.VERSION, settings.BUILD_NUMBER),
    }
