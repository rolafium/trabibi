const path = require("path");
const webpack = require("webpack");
const VueLoaderPlugin = require('vue-loader/lib/plugin')


const mode = process.env.NODE_ENV || "development";


module.exports = {
    devtool : "cheap-module-source-map",
    entry : __dirname + "/src/main.js",
    mode: mode,
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: "vue-loader",
            },
            {
                test: /\.css$/,
                use: [
                    "vue-style-loader",
                    "css-loader",
                ]
            },
            {
                test: /\.js$/,
                use: { 
                    loader: "istanbul-instrumenter-loader",
                    options: { esModules: true },
                },
                include: path.resolve("src/"),
            }
        ]
    },
    output : {
        path: __dirname + "/build",
        filename: "trabibi.js",
    },
    plugins: [
        new VueLoaderPlugin(),
    ],
}
