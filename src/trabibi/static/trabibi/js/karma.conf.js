const webpackConfig = require("./webpack.config.js");

// Sets the mode to development for performance
webpackConfig.mode = "development";
// Removes the cheap source-maps as we need to see
// the lines where the code fails
delete webpackConfig.devtool;

module.exports = function(config) {
    config.set({
        browsers: ['ChromeHeadless'],
        colors: true,
        files: [
            'src/**/*.js',
            'tests/**/*.js',
        ],
        frameworks: ['mocha', 'chai'],
        logLevel: config.LOG_INFO,
        port: 1111,
        preprocessors: {
            'src/**/*.js': ['webpack', 'coverage'],
            'tests/**/*.js': ['webpack']
        },
        reporters: ["spec", "coverage-istanbul"],
        coverageIstanbulReporter: {
            reports: [ "text-summary", "html" ],
            fixWebpackSourcePaths: true,
            dir: "./build/coverage",
        },
        singleRun: true,
        concurrency: Infinity,
        webpack: webpackConfig,
    });
};
