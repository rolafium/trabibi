import chai from "chai";
import testSchema from "./db/mock_schema.js";
import storeConfig from "../src/storeConfig.js";
import routerConfig from "../src/routerConfig.js";
import getSetup from "../src/setup.js";


describe("Setup", () => {

    const config = {
        schema: testSchema.TEST_SCHEMA,
        store: storeConfig,
        router: routerConfig,
    };
    const setup = getSetup(config);

    it("Should have all the required components", () => {
        expect(setup.Vue).to.not.be.undefined;
        expect(setup.appContext).to.not.be.undefined;
        expect(setup.store).to.not.be.undefined;
        expect(setup.router).to.not.be.undefined;
    });

    it("Vue should have a series of prototypes", () => {
        expect(setup.Vue.prototype.context).to.not.be.undefined;
        expect(setup.Vue.prototype.$ajax).to.not.be.undefined;
        expect(setup.Vue.prototype.$cookie).to.not.be.undefined;
    });

    it("A setup.Vue instance should have $router and $store defined", () => {
        var vueInstance = new setup.Vue({ 
            store: setup.store, 
            router: setup.router,
        });
        expect(vueInstance.$store).to.not.be.undefined;
        expect(vueInstance.$router).to.not.be.undefined;
    });

    it("The context should have a series of properties", () => {
        expect(setup.appContext.migrations).to.not.be.undefined;
        expect(setup.appContext.db).to.not.be.undefined;
        expect(setup.appContext.cookie).to.not.be.undefined;
        expect(setup.appContext.ajax).to.not.be.undefined;
    });

});
