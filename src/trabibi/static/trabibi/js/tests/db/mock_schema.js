import schema from "../../src/db/migrations/schema.js";


const TEST_DB = "TRABIBI_TEST_INDEXED_DB";

const TEST_TABLES_1 = [
    {
        name: "tracking_updates",
        fields: [
            "++id",
            "obj",
            "relative_time",
            "value",
            "notes",
        ],
    },
];

const TEST_TABLES_2 = [
    {
        name: "tracking_updates",
        fields: [
            "++id",
            "obj",
            "relative_time",
            "values",
            "description",
        ]
    },
    {
        name: "tracking_details",
        fields: [
            "++id",
            "obj",
            "notes",
        ]
    },
]

const schema_upgrade_2 = (transaction) => {
    var tu = transaction.tracking_update;
    tu.description = tu.notes;
    delete tu.notes;
};

const TEST_SCHEMA = {
    dbName: TEST_DB,
    currentVersion: 1,
    migrations: [
        {
            version: 1,
            store: new schema.MigrationStore(TEST_TABLES_1),
            upgrade: null,
        },
        {
            version: 2,
            store: new schema.MigrationStore(TEST_TABLES_2),
            upgrade: schema_upgrade_2,
        }
    ]
}

export default {
    TEST_DB,
    TEST_TABLES_1,
    TEST_TABLES_2,
    TEST_SCHEMA,
}
