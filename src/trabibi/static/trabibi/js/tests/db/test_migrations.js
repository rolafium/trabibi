import chai from "chai";
import Dexie from "dexie";
import schema from "../../src/db/migrations/schema.js";
import testSchema from "./mock_schema.js";
import MigrationManager from "../../src/db/migrations/manager.js";

const assert = chai.assert;
const expect = chai.expect

describe('Migrations', () => {

    describe("MigrationStore", () => {

        const store = new schema.MigrationStore(testSchema.TEST_TABLES_1);

        it("Should possess a table attribute", () => {
            expect(store.tables).to.deep.equal(testSchema.TEST_TABLES_1);
        });

        it("Should produce a Dexie schema", () => {
            const schema = store.dexieSchema;
            const expectedSchema = "++id,obj,relative_time,value,notes";
            expect(schema.tracking_updates).to.equal(expectedSchema);
        });

    });

    describe("MigrationManager", () => {

        before((done) => {
            // Delete the IndexedDB test db
            // so we can test migrations from scratch
            var deleteRequest = window.indexedDB.deleteDatabase(testSchema.TEST_DB);
            deleteRequest.onerror = (event) => {
                throw Error("Could not delete the database");
            }
            deleteRequest.onsuccess = (event) => {
                done();
            }
        });

        const manager = new MigrationManager(testSchema.TEST_SCHEMA);

        it("Should map the schema properties", () => {
            expect(manager.schema).to.deep.equal(testSchema.TEST_SCHEMA);
            expect(manager.dbName).to.equal(testSchema.TEST_SCHEMA.dbName);
            expect(manager.version).to.equal(testSchema.TEST_SCHEMA.currentVersion);
        });

        it("Should retrieve the migration for a specific version", () => {
            expect(manager.getMigration(1)).to.deep.equal(testSchema.TEST_SCHEMA.migrations[0])
            
            const badCall = () => manager.getMigration(999);
            expect(badCall).to.throw(Error);
        });

        it("Should run the migrations", () => {
            const db = new Dexie(testSchema.TEST_DB);
            manager.migrate(db);
            expect(db._dbSchema.tracking_updates).to.not.be.undefined;
            expect(db._dbSchema.tracking_details).to.not.be.undefined;
            expect(db._dbSchema.xxxx).to.be.undefined;
        });


    });

});
