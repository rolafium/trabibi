import storeConfig from "../src/storeConfig.js";

describe("StoreConfig", () => {

    const state = storeConfig.state;
    const commit = storeConfig.mutations;

    it("Should have a list of properties", () => {
        expect(state.page).to.not.be.undefined;
        expect(state.page.h1).to.not.be.undefined;
        expect(state.tracking).to.not.be.undefined;
        expect(state.tracking.objects).to.not.be.undefined;
        expect(state.tracking.details).to.not.be.undefined;
        expect(state.tracking.updates).to.not.be.undefined;
        expect(state.tracking.graphs).to.not.be.undefined;
    });

    it("Should set tracking", () => {
        let _state = JSON.parse(JSON.stringify(state));
        let _object = [1]
        commit.setTracking(_state, { name: "objects", object: [1] });
        commit.setTracking(_state, { name: "details", object: [2] });
        commit.setTracking(_state, { name: "updates", object: [3] });
        commit.setTracking(_state, { name: "graphs", object: [4] });

        expect(_state.tracking.objects).to.deep.equal([1]);
        expect(_state.tracking.details).to.deep.equal([2]);
        expect(_state.tracking.updates).to.deep.equal([3]);
        expect(_state.tracking.graphs).to.deep.equal([4]);
    });

    it("Should add, edit and delete tracking", () => {

        const _testTracking = (name) => {
            let _state = JSON.parse(JSON.stringify(state));
            let objectID = 9;
            let object = { id: objectID, data: "object" };
            let objectVariation = { id: objectID, data: "variation" };

            commit.addTracking(_state, { name, object });
            expect(_state.tracking[name][0]).to.deep.equal(object);

            commit.editTracking(_state, { name, object: objectVariation });
            expect(_state.tracking[name][0]).to.deep.equal(objectVariation);

            let badFunc = () => commit.deleteTracking(_state, { name, object });
            expect(badFunc).to.throw(RangeError);

            commit.deleteTracking(_state, { name, object: objectVariation });
            expect(_state.tracking[name].length).to.equal(0);
        }

        _testTracking("objects");
        _testTracking("details");
        _testTracking("updates");
        _testTracking("graphs");
    });

});
