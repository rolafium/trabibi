import chai from "chai";
import utils from "../src/utils.js";

const assert = chai.assert;
const expect = chai.expect
const CookieManager = utils.CookieManager;


describe('JavaScript Utils', () => {
    describe('CookieManager', () => {
        const cm = new CookieManager();
        const cookie = {
            name: "test-name",
            value: "test-value",
        };
        const expectedCookie = cookie.name + "=" + cookie.value;

        it('Should set a cookie', () => {
            cm.set(cookie);
            expect(document.cookie).to.include(expectedCookie)
        });

        it("Should retrieve a cookie", () => {
            cm.set(cookie);
            var cookieValue = cm.get(cookie.name);
            expect(cookieValue).to.equal(cookie.value);
        });

        it("Should delete a setted cookie", () => {
            cm.set(cookie);
            expect(document.cookie).to.include(expectedCookie);
            cm.delete(cookie.name);
            expect(document.cookie).to.not.include(expectedCookie);
        });

    });
});
