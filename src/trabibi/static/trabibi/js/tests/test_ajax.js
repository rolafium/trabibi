import chai from "chai";
import utils from "../src/utils.js";
import ajax from "../src/ajax.js";


describe('Ajax', () => {

    describe("AjaxWrapper", () => {
        it("Should map in properties the xhr properties", () => {
            const xhr = new XMLHttpRequest();
            const ajaxw = new ajax.AjaxWrapper(xhr, "/", "GET");

            expect(ajaxw.status).to.equal(xhr.status, "Check status");
            expect(ajaxw.statusText).to.equal(xhr.statusText, "Check statusText");
            expect(ajaxw.readyState).to.equal(xhr.readyState, "Check readyState");
            expect(ajaxw.rawResponse).to.equal(xhr.response, "Check response");
            expect(ajaxw.rawResponseText).to.equal(xhr.responseText, "Check responseText");
        });

        it("Should read the json from the xhr", () => {
            const data = { a: 1, b: 2, c: { d: 4, e: 5 } };
            const datastr = JSON.stringify(data);
            const fakeXhr = {
                status: 200,
                statusText: "OK",
                readyState: 4,
                response: datastr,
                responseText: datastr,
            }
            const ajaxw = new ajax.AjaxWrapper(fakeXhr, "/", "GET");

            expect(ajaxw.json).to.deep.equal(data);
        });
    });

    describe('AjaxRequestFactory', () => {

        const cm = new utils.CookieManager();
        cm.set({ name: "csrftoken", value: "test" });

        const aj = new ajax.AjaxRequestFactory({ cookie: cm });
        const safeMethods = ["GET", "HEAD", "OPTIONS"];
        const unsafeMethods = ["POST", "PUT", "PATCH", "DELETE"];

        it('Should have proper defined safe methods', () => {
            safeMethods.forEach((method) => expect(aj.safeMethods).to.include(method));
            unsafeMethods.forEach((method) => expect(aj.safeMethods).to.not.include(method));
        });

        it("Should recognise a safe method", () => {
            safeMethods.forEach((method) => assert.isTrue(aj._isSafeMethod(method), "Method should be safe"));
            unsafeMethods.forEach((method) => assert.isFalse(aj._isSafeMethod(method), "Method should be unsafe"));
        });

        it("Should build a proper query string", () => {
            const data = { firstName: "William", lastName: "Di Pasquale" };
            const expectedQs = "?firstName=William&lastName=Di%20Pasquale";
            const qs = aj._getQueryString(data);
            expect(qs).to.equal(expectedQs);
        });

        it("Should return a configured AjaxWrapper", () => {
            var ajaxw1 = aj._createAjaxWrapper({
                url: "/",
                method: "POST",
            });

            expect(ajaxw1.constructor.name).to.equal("AjaxWrapper");
            expect(ajaxw1.url).to.equal("/");
            expect(ajaxw1.method).to.equal("POST");

            var ajaxw2 = aj._createAjaxWrapper({
                url: "/some-url",
                method: "GET",
                data: { firstName: "William", lastName: "Di Pasquale" },
            });

            expect(ajaxw2.constructor.name).to.equal("AjaxWrapper");
            expect(ajaxw2.url).to.equal("/some-url?firstName=William&lastName=Di%20Pasquale");
            expect(ajaxw2.method).to.equal("GET");
        });

    });
});
