

/**
 * Wrapper over an XMLHttpRequest
 */
class AjaxWrapper {

    /**
     * Builds an AjaxWrapper from an XMLHttpRequest object
     * @param {XMLHttpRequest} xhr
     * @param {string} url
     * @param {string} method
     */
    constructor(xhr, url, method) {
        this.xhr = xhr;
        this.url = url;
        this.method = method;
    }

    /** Gets the status of the xhr */
    get status() {
        return this.xhr.status;
    }

    /** Gets the status of the xhr */
    get statusText() {
        return this.xhr.statusText;
    }

    /** Gets the status of the xhr */
    get readyState() {
        return this.xhr.readyState;
    }

    /** Gets the status of the xhr */
    get rawResponse() {
        return this.xhr.response;
    }

    /** Gets the status of the xhr */
    get rawResponseText() {
        return this.xhr.responseText;
    }

    /**
     * Generates a JSON from the response body
     * @returns {object}
     */
    get json() {
        return JSON.parse(this.xhr.response);
    }

}

/**
 * Issues XMLHttpRequest with pre-populated values
 */
class AjaxRequestFactory {

    /**
     * Builds an AjaxRequestFactory object
     * @param context {object} AppContext (see ./main.js)
     */
    constructor(context={}) {
        this.context = context;
        this.context.ajax = this;
        this.safeMethods = [
            "GET",
            "HEAD",
            "OPTIONS",
            "TRACE",
        ];
    }

    /**
     * Checks if the request uses a safe method
     * @param method {string}
     * @returns boolean
     */
    _isSafeMethod(method) {
        return this.safeMethods.indexOf(method.toUpperCase()) > - 1;
    }

    /**
     * Creates and encodes the querystring
     * @param data {object}
     * @returns string
     */
    _getQueryString(data) {
        if(data) {
            var components = [];
            for(var key in data) {
                var query = `${key}=${data[key]}`
                components.push(query);
            }
            return encodeURI("?" + components.join("&"));
        }
        return "";
    }

    /**
     * Prepares the XMLHttpRequest in the AjaxWrapper
     * @param config {object} Specification of the request
     * @returns AjaxWrapper
     */
    _createAjaxWrapper(config) {
        var csrftoken = this.context.cookie.get("csrftoken");
        var xhr = new XMLHttpRequest();

        config.method = config.method ? config.method.toUpperCase() : "GET";
        config.preset = config.preset ? config.preset.toUpperCase() : "JSON";

        var isSafe = this._isSafeMethod(config.method);
        if(isSafe) {
            config.url += this._getQueryString(config.data);
        }

        xhr.open(config.method, config.url, true);
        xhr.setRequestHeader("X-CSRFToken", csrftoken);

        if(config.preset === "JSON") {
            xhr.setRequestHeader("Content-Type", "application/json");
            config.data = JSON.stringify(config.data);
        }

        var ajaxw = new AjaxWrapper(xhr, config.url, config.method);
        return ajaxw;
    }
    /**
     * Issues a XMLHttpRequest
     * @param config {object} Specification of the request
     * @returns Promise(AjaxWrapper)
     */
    request(config) {
        return new Promise((resolve, reject) => {
            var ajaxw = this._createAjaxWrapper(config);
            ajaxw.xhr.addEventListener("load", (e) => resolve(ajaxw));

            if(config.data) {
                ajaxw.xhr.send(config.data)
            } else {
                ajaxw.xhr.send();
            }
        });
    }
}

export default {
    AjaxWrapper,
    AjaxRequestFactory,
}
