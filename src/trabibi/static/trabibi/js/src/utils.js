

/**
 * Cookies manager with get, set, delete
 */
class CookieManager {

    /**
     * Checks if a value is null like (null, undefined)
     * @private
     * @param value
     * @returns {boolean}
     */
    _isNullLike(value) {
        return value === null || value === undefined;
    }

    /**
     * Sets a cookie
     * @param config {object}
     * Config object with required keys (name, value) and optional values (ms, path)
     * @throws {Error} If one of the required keys is missing
     */
    set(config) {
        if(this._isNullLike(config.name) || this._isNullLike(config.value)) {
            throw Error("`name` and `value` are required keys.");
        }

        var components = [];

        components.push(`${config.name}=${config.value}`);

        if(config.ms !== 0) {
            var date = new Date();
            date.setTime(date.getTime() + (config.ms || 25 * 60 * 60 * 1000))
            components.push(`expires=${date.toGMTString()}`);
        }

        components.push(`path=${config.path || "/"}`);

        document.cookie = components.join(";");
    }

    /**
     * Gets a cookie by its name, returns null if not found
     * @param {string} cookieName
     * @returns {string|null}
     */
    get(cookieName) {
        var regex = new RegExp("(?:^|;)\\s?" + cookieName + "=(.*?)(?:;|$)");
        var match = document.cookie.match(regex);

        return match ? match[1] : null;
    }

    /**
     * Deletes a cookie by setting its expiring date in the past
     * @param {string} cookieName
     */
    delete(cookieName) {
        this.set({
            name: cookieName,
            value: "",
            ms: -1,
        });
    }

}

export default {
    CookieManager,
}
