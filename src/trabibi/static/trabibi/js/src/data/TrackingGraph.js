

class TrackingGraph {

    constructor(instance, updates) {
        this._instance = instance;
        this._updates = updates;
        this.trackManager = new GraphTracksManager(this._instance.tracked);
        this.trackManager.loopUpdates(updates);
    }

    get sum() {
        return this.trackManager.createCoordinates(this.trackManager.sum);
    }

    get tracks() {
        var tracks = { sum: this.sum };
        this.trackManager.tracked.forEach((track) => {
            var trackDetails = this.trackManager.tracks[track];
            tracks[track] = this.trackManager.createCoordinates(trackDetails);
        })

        return tracks;
    }

}

class GraphTracksManager {

    constructor(tracked) {
        this.tracked = tracked;
        this.sum = {};
        this.tracks = {};
        this.tracked.forEach((track) => this.tracks[track] = new GraphTrack(track));
    }

    registerUpdate(update) {
        this.tracks[update.obj].register(update);
        this.registerSum(update.relative_time);
    }

    registerSum(timecode) {
        let sum = 0;
        for(var trackId in this.tracks) {
            sum += this.tracks[trackId].lastUpdate;
        }
        this.sum[timecode] = sum;
    }

    loopUpdates(updates) {
        updates.forEach((update) => this.registerUpdate(update));
    }

    createCoordinates(obj) {
        return Object.keys(obj).map((timecode) => {
            return {
                date: parseInt(timecode),
                value: obj[timecode],
            }
        });
    }
}

class GraphTrack {
    constructor(id) {
        this.id = id;
        this.timecodes = {};
        this.lastUpdate = 0;
    }

    register(update) {
        var value = parseFloat(update.value);
        this.timecodes[update.relative_time] = value;
        this.lastUpdate = value;
    }
}

export default TrackingGraph;
