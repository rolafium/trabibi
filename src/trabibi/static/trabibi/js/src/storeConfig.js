import Vue from "vue";

export default {
    state: {
        user: window.user,
        page: {
            h1: "Trabibi",
        },
        tracking: {
            objects: [],
            details: [],
            updates: [],
            graphs: []
        },
    },
    mutations: {
        /** 
         * Sets the authentication value of a user
         */
        auth(state, value) {
            state.user.auth = value;
        },
        /**
         * Updates the user information
         */
        updateUser(state, data) {
            if(!data) {
                data = {};
            }
            state.user.id = data.id || 0;
            state.user.username = data.username || "";
            state.user.email = data.email || "";
            state.user.firstName = data.first_name || "";
            state.user.lastName = data.last_name || "";
        },
        /**
         * Sets the H1 of the page
         */
        updateH1(state, data) {
            state.page.h1 = data
        },
        /**
         * Sets the tracking elements (objects, details, updates, graphs)
         */
        setTracking(state, data) {
            state.tracking[data.name] = data.object;
        },
        /**
         * Add a tracking element (objects, details, updates, graphs)
         */
        addTracking(state, data) {
            state.tracking[data.name].push(data.object);
        },
        /**
         * Edits a tracking element (objects, details, updates, graphs)
         */
        editTracking(state, data) {
            var index = state.tracking[data.name].map((obj) => obj.id).indexOf(data.object.id);
            Vue.set(state.tracking[data.name], index, data.object);
        },
        /**
         * Deletes a tracking element (objects, details, updates, graphs)
         */
        deleteTracking(state, data) {
            var index = state.tracking[data.name].indexOf(data.object);
            if(index === -1) {
                throw RangeError("Could not find " + data.name);
            }
            state.tracking[data.name].splice(index, 1);
        }
    }
}
