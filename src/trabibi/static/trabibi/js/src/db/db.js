import Dexie from 'dexie';

const createDB = () => {
    const db = new Dexie(window.FRONTEND_DB_NAME);
    return db;
}

export default createDB;
