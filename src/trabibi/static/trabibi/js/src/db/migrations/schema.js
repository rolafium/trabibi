

/**
 * Wrapper over a list of tables
 */ 
class MigrationStore {
    /**
     * Creates a Migration Store
     * @param {array} tables Array of tables to be created
     */
    constructor(tables) {
        this.tables = tables;
    }

    /**
     * Generates the schema as expected by dexie
     */
    get dexieSchema() {
        const schema = {}
        this.tables.forEach((table) => {
            schema[table.name] = table.fields.join(",");
        });
        return schema;
    }
}

/**
 * Current IndexedDB/Dexie Schema
 */
const SCHEMA = {
    dbName: "TRABIBI_INDEXED_DB",
    currentVersion: 1,
    migrations: [
        {
            version: 1,
            store: new MigrationStore([
                {
                    name: "tracking_updates",
                    fields: [
                        "++id",
                        "obj",
                        "relative_time",
                        "value",
                        "notes"
                    ],
                },
            ]),
            upgrade: null,
        },
    ],
};


export default {
    SCHEMA,
    MigrationStore
};
