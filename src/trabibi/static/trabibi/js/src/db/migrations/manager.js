
/**
 * IndexedDB Migration Manager
 *
 * Helps with migrations/upgrades of the database
 */
class MigrationManager {

    /**
     * Creates a migration manager
     * @param {object} schema Object as specified in ./schema.js
     */
    constructor(schema) {
        this.schema = schema;
    }

    /**
     * Returns the db name
     */
    get dbName() {
        return this.schema.dbName;
    }

    /**
     * Returns the db current version
     */
    get version() {
        return this.schema.currentVersion;
    }

    /**
     * Returns an individual migration
     * @param {integer} versionNumber
     * @throws {Error} Error when the migration is not found
     * @returns {object}
     */
    getMigration(versionNumber) {
        for(var i = 0; i < this.schema.migrations.length; i++) {
            var migration = this.schema.migrations[i]
            if(migration.version === versionNumber) {
                return migration;
            }
        }
        throw Error(`Migration ${versionNumber} not found.`)
    }

    /**
     * Migrates the indexed db to the latest version
     * @param db Dexie db instance
     * @returns migrated Dexie db instance
     */
    migrate(db) {
        /** Ensures the migrations are ordered asc */
        const migrations = this.schema.migrations.sort((m1, m2) => m1.version > m2.version ? 1 : -1);

        migrations.forEach((migration) => {
            var dbVersion = db
            .version(migration.version)
            .stores(migration.store.dexieSchema)

            if(migration.upgrade) {
                dbVersion.upgrade(migration.upgrade);
            }
        });

        return db;
    }
}

export default MigrationManager;
