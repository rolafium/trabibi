import schema from "./db/migrations/schema.js";
import storeConfig from "./storeConfig.js";
import routerConfig from "./routerConfig.js";
import getSetup from "./setup.js";

import TrabibiApp from "./vue/TrabibiApp.vue";

storeConfig.state.user = window.USER;

const config = {
    schema: schema.SCHEMA,
    store: storeConfig,
    router: routerConfig,
}

const setup = getSetup(config);

const app = new setup.Vue({
    el: "#trabibi-root",
    components: { trabibiApp: TrabibiApp },
    store: setup.store,
    router: setup.router,
});

window.setup = setup;
window.appContext = setup.appContext;
window.app = app;
