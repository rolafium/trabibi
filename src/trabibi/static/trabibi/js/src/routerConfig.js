
const load = (component) => require(`./vue/${component}.vue`).default;
const createRoute = (path, component, routeName) => {
    return {
        path: path,
        component: load(component),
        name: routeName,
    };
}

export default {
    mode: "history",
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        createRoute("/", "Landing", "landing"),
        createRoute("/tracking-objects/:id", "TrackingObject", "tracking-objects"),
    ]
}
