import MigrationManager from "./db/migrations/manager.js";
import createDB from "./db/db.js";
import ajax from "./ajax.js";
import utils from "./utils.js";

import Vue from "vue/dist/vue.esm.js";
import Vuex from "vuex";
import VueRouter from "vue-router";


const getSetup = (config) => {
    const appContext = {};
    appContext.migrations = new MigrationManager(config.schema);
    appContext.db = appContext.migrations.migrate(createDB());
    appContext.cookie = new utils.CookieManager();
    appContext.ajax = new ajax.AjaxRequestFactory(appContext);

    Vue.use(Vuex);
    Vue.use(VueRouter);

    Vue.prototype.context = appContext;
    Vue.prototype.$ajax = appContext.ajax;
    Vue.prototype.$cookie = appContext.cookie;

    const store = new Vuex.Store(config.store);
    const router = new VueRouter(config.router);

    return {
        Vue,
        appContext,
        store,
        router,
    };
}

export default getSetup;

