import json
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db.utils import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django_request_cop.models import GoogleRecaptchaRequestBlock
from trabibi.models import TrackingObject, TrackingDetails, TrackingUpdate, TrackingGraph
from trabibi.serializers import (
    UserSerializer,
    UserLoginSerializer,
    TrackingObjectSerializer,
    TrackingObjectDetailsSerializer,
    TrackingDetailsSerializer, 
    TrackingUpdateSerializer, 
    TrackingGraphSerializer,
)


class UserViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    base_name = "users"

    def create(self, request):
        try:
            GoogleRecaptchaRequestBlock.validate(
                request, 
                "TRABIBI_USER_REGISTRATION", 
                google_response=request.data.get("google_response", None),
            )
        except AttributeError:
            raise PermissionDenied("google_response is a required field.")

        response = {
            "created": False,
            "user": None
        }

        if request.user.is_authenticated:
            response["message"] = "Only logged out users can register."
            return Response(response, status=status.HTTP_412_PRECONDITION_FAILED)

        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            response["message"] = "Data is invalid"
            response["errors"] = serializer.errors
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        user = None
        try:
            user = serializer.save()
        except IntegrityError:
            response["message"] = "User with {email} already exists".format(
                email=serializer.validated_data["email"],
            )
            return Response(response, status=status.HTTP_409_CONFLICT)

        login(request, user)

        response["created"] = True
        response["user"] = serializer.data

        return Response(response, status=status.HTTP_201_CREATED)

    @action(methods=["post"], detail=False, serializer_class=UserLoginSerializer)
    def login(self, request):
        try:
            GoogleRecaptchaRequestBlock.validate(
                request, 
                "TRABIBI_USER_LOGIN", 
                google_response=request.data.get("google_response", None),
            )
        except AttributeError:
            raise PermissionDenied("google_response is a required field.")

        response = {
            "login": False,
            "user": None
        }

        serializer = UserLoginSerializer(data=request.data)

        if not serializer.is_valid():
            response["message"] = "Invalid request."
            response["errors"] = serializer.errors
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(request, **serializer.validated_data)

        if not user:
            response["message"] = "User not found."
            return Response(response, status=status.HTTP_404_NOT_FOUND)

        login(request, user)
        user_data = UserSerializer(user)

        response["login"] = True
        response["user"] = user_data.data

        return Response(response, status=status.HTTP_200_OK)

    @action(methods=["get", "post"], detail=False)
    def logout(self, request):
        response = {
            "logout": False,
        }

        if not request.user.is_authenticated:
            response["message"] = "You are already logged out."
            return Response(response, status=status.HTTP_412_PRECONDITION_FAILED)

        logout(request)

        response["logout"] = True

        return Response(response, status=status.HTTP_200_OK)

    def get_queryset(self):
        """Queryset filters based on superuser status"""
        user = self.request.user
        queryset = self.queryset

        if not user.is_authenticated:
            return queryset.none()
        
        elif not user.is_superuser:
            return queryset.filter(pk=user.id)

        else:
            return queryset


class TrackingObjectViewset(viewsets.ModelViewSet):
    queryset = TrackingObject.objects.all()
    serializer_class = TrackingObjectSerializer
    permission_classes = (IsAuthenticated,)
    base_name = "trackingobjects"

    def get_serializer_class(self):
        if self.request.method == "GET" and "details" in self.request.GET:
            return TrackingObjectDetailsSerializer
        else:
            return self.serializer_class

    def get_queryset(self):
        """Returns only the tracking objects related to the request user"""
        user = self.request.user
        return self.queryset.filter(user=user)

    

class TrackingDetailsViewset(viewsets.ModelViewSet):
    queryset = TrackingDetails.objects.all()
    serializer_class = TrackingDetailsSerializer
    permission_classes = (IsAuthenticated,)
    base_name = "trackingdetails"

    def get_queryset(self):
        """Returns only the tracking details related to the request user"""
        user = self.request.user
        return self.queryset.filter(obj__user=user)


class TrackingUpdateViewset(viewsets.ModelViewSet):
    queryset = TrackingUpdate.objects.all()
    serializer_class = TrackingUpdateSerializer
    permission_classes = (IsAuthenticated,)
    base_name = "trackingupdates"

    def get_queryset(self):
        """Returns only the tracking updates related to the request user"""
        user = self.request.user
        return self.queryset.filter(obj__user=user)


class TrackingGraphViewset(viewsets.ModelViewSet):
    queryset = TrackingGraph.objects.all()
    serializer_class = TrackingGraphSerializer
    permission_classes = (IsAuthenticated,)
    base_name = "trackinggraphs"

    def get_queryset(self):
        """Returns only the tracking graphs related to the request user"""
        user = self.request.user
        return self.queryset.filter(parent__user=user)

def redirect_to_api(request):
    return HttpResponseRedirect("/api/")

def landing(request):
    user = {}
    if request.user.is_authenticated:
        user = {
            "id": request.user.id,
            "auth": True,
            "first_name": request.user.first_name,
            "last_name": request.user.last_name,
            "email": request.user.email,
        }
    else:
        user = {
            "id": 0,
            "auth": False,
            "first_name": "",
            "last_name": "",
            "email": "",
        }

    context = {
        "user": json.dumps(user)
    }

    return render(request, "trabibi/landing.html", context)
