from django.test import TestCase
from trabibi.serializers import UserLoginSerializer, UserSerializer


class TestSerializers(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.email = "wdp+test_serializers@rolafium.com"
        cls.password = "some_fake_password"


    @classmethod
    def tearDownClass(cls):
        pass


    def test_user_login_serializer(self):
        """Checks if the data is valid and processed correctly"""
        invalid_data1 = {
            "email": self.email,
        }
        invalid_data2 = {
            "password": self.password,
        }

        valid_data = {
            "email": self.email,
            "password": self.password
        }


        self.assertFalse(UserLoginSerializer(data=invalid_data1).is_valid())
        self.assertFalse(UserLoginSerializer(data=invalid_data2).is_valid())

        serializer = UserLoginSerializer(data=valid_data)
        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data["username"], self.email)


    def test_user_serializer(self):
        """Checks if the data is valid and processed correctly"""
        invalid_data = {
            "email": self.email,
        }

        valid_data = {
            "first_name": "A",
            "last_name": "B",
            "email": self.email,
            "password": self.password,
        }

        self.assertFalse(UserSerializer(data=invalid_data).is_valid())

        serializer = UserSerializer(data=valid_data)
        self.assertTrue(serializer.is_valid())
        user = serializer.save()

        self.assertTrue(isinstance(user.id, int))
        self.assertEqual(user.username, self.email)
        self.assertEqual(user.email, self.email)
