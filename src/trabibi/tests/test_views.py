import json
from django.core.exceptions import PermissionDenied
from django.test import TestCase, Client, override_settings
from django.test.client import RequestFactory
from rest_framework.reverse import reverse
from django_request_cop.models import GoogleRecaptchaRequestBlock
from django_request_cop.tests import override_settings_and_recaptcha
from trabibi.views import (
    landing,
    redirect_to_api,
    TrackingObjectViewset,
    TrackingDetailsViewset,
    TrackingUpdateViewset,
    TrackingGraphViewset,
    UserViewset,
)
from trabibi.tests.utils import get_or_create_test_user, get_or_create_tracking_object


class TestViews(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.rf = RequestFactory()
        cls.user = get_or_create_test_user("TestViews")
        cls.user2 = get_or_create_test_user("TestViews2")
        cls.tracking_object = get_or_create_tracking_object("TestViews")

        cls.email = "wdp+test_views@rolafium.com"
        cls.password = "some_fake_password"

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.client = Client()

    def assert_get_response_code_is(self, viewFunc, code=200):
        """Asserts that a get request to the viewFunc returns code"""
        url = reverse(viewFunc)
        response = self.client.get(url)
        self.assertEqual(response.status_code, code)
        return response

    def assert_post_response_code_is(self, viewFunc, data, code=200):
        """Asserts that a post request to the viewFunc returns code"""
        url = reverse(viewFunc)
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, code)
        return response

    def assert_object_in_queryset(self, view, data, assertion=False):
        obj, created = view.serializer_class.Meta.model.objects.get_or_create(**data)
        url = reverse(view.base_name + "-list")
        request = self.rf.get(url)
        request.user = self.user2
        view_instance = view()
        view_instance.request = request
        queryset = view_instance.get_queryset()
        self.assertEqual(obj in queryset, assertion)
        return queryset

    def test_landing(self):
        """Tests the view"""
        self.assert_get_response_code_is(landing, code=200)
        url = reverse(landing)

        res1 = self.client.get(url)
        context_user = json.loads(res1.context["user"])
        self.assertEqual(context_user["id"], 0)
        self.assertFalse(context_user["auth"])

        self.client.force_login(self.user)
        res2 = self.client.get(url)
        context_user2 = json.loads(res2.context["user"])
        self.assertEqual(context_user2["id"], self.user.id)
        self.assertTrue(context_user2["auth"])

    def test_redirect_to_api(self):
        """Tests the redirect"""
        self.assert_get_response_code_is(redirect_to_api, code=302)

    def test_tracking_object_viewset(self):
        """Tests the view"""
        self.assert_get_response_code_is(TrackingObjectViewset.base_name + "-list", code=403)
        self.client.force_login(self.user)
        self.assert_get_response_code_is(TrackingObjectViewset.base_name + "-list", code=200)

        good_object_data = { "name": "TRACKING_OBJECT_TEST", "user": self.user.id }
        self.assert_post_response_code_is(TrackingObjectViewset.base_name + "-list", {}, code=400)
        self.assert_post_response_code_is(TrackingObjectViewset.base_name + "-list", good_object_data, code=201)

        self.assert_object_in_queryset(TrackingObjectViewset, { 
            "name": "a", 
            "user_id": self.user.id
        }, False)

    def test_tracking_details_viewset(self):
        """Tests the view"""
        self.assert_get_response_code_is(TrackingDetailsViewset.base_name + "-list", code=403)
        self.client.force_login(self.user)
        self.assert_get_response_code_is(TrackingDetailsViewset.base_name + "-list", code=200)

        self.assert_object_in_queryset(TrackingDetailsViewset, { 
            "name": "a", 
            "obj_id": self.tracking_object.id, 
            "value": "",
        }, False)

    def test_tracking_update_viewset(self):
        """Tests the view"""
        self.assert_get_response_code_is(TrackingUpdateViewset.base_name + "-list", code=403)
        self.client.force_login(self.user)
        self.assert_get_response_code_is(TrackingUpdateViewset.base_name + "-list", code=200)

        self.assert_object_in_queryset(TrackingUpdateViewset, { 
            "obj_id": self.tracking_object.id, 
            "value": 1,
        }, False)


    def test_tracking_graph_viewset(self):
        """Tests the view"""
        self.assert_get_response_code_is(TrackingGraphViewset.base_name + "-list", code=403)
        self.client.force_login(self.user)
        self.assert_get_response_code_is(TrackingGraphViewset.base_name + "-list", code=200)

        self.assert_object_in_queryset(TrackingGraphViewset, { 
            "name": "test", 
            "parent_id": self.tracking_object.id, 
            "operation": "SUM",
        }, False)

    @override_settings_and_recaptcha()
    def test_user_viewset(self):
        """Tests the view"""
        user_list_path = UserViewset.base_name + "-list"
        self.assert_get_response_code_is(user_list_path, code=200)

        bad_object_data = {
            "email": self.email,
            "google_response": "GOOGLE_RESPONSE",
        }

        good_object_data = {
            "email": self.email,
            "password": self.password,
            "google_response": "GOOGLE_RESPONSE",
        }


        response = self.assert_post_response_code_is(user_list_path, bad_object_data, code=400).json()
        self.assertFalse(response["created"])

        response = self.assert_post_response_code_is(user_list_path, good_object_data, code=201).json()
        self.assertTrue(response["created"])
        self.assertEqual(response["user"]["username"], self.email)
        self.assertEqual(response["user"]["email"], self.email)

        # Only logged out users can create a user
        # and we have been logged in by creating a user
        response = self.assert_post_response_code_is(user_list_path, good_object_data, code=412).json()

        self.client.logout()
        # Can't create a user twice
        response = self.assert_post_response_code_is(user_list_path, good_object_data, code=409).json()
        self.assertFalse(response["created"])

    def test_user_viewset_create_spam(self):
        """Tests if the viewset gets called over the limit"""
        users = [
            { 
                "email": str(n) + "_test_" + self.email,
                "password": self.password,
                "google_response": "INVALID_RESPONSE",
            }
            for n in range(GoogleRecaptchaRequestBlock.MAX_REQUESTS + 1)
        ]

        def _post_create_and_logout(user_data):
            self.assert_post_response_code_is(UserViewset.base_name + "-list", user_data, code=403).json()
            self.client.logout()

        for i in range(GoogleRecaptchaRequestBlock.MAX_REQUESTS):
            _post_create_and_logout(users[i])

        self.assert_post_response_code_is(UserViewset.base_name + "-list", users[-1], code=403)


    def test_user_viewset_get_queryset(self):
        """Tests the get_queryset method"""
        user_queryset_fail_data = { "email": self.user.email }
        user_queryset_success_data = { "email": self.user2.email }

        self.assert_object_in_queryset(UserViewset, user_queryset_fail_data, False)
        self.assert_object_in_queryset(UserViewset, user_queryset_success_data, True)
        
        # user2 is the request user
        self.user2.is_superuser = True
        self.assert_object_in_queryset(UserViewset, user_queryset_fail_data, True)
        self.user2.is_superuser = False


    @override_settings_and_recaptcha()
    def test_user_viewset_login_and_logout(self):
        """Tests the view login and logout actions"""
        login_path = UserViewset.base_name + "-login"
        logout_path = UserViewset.base_name + "-logout"

        # Can't logout if you are already out
        response = self.assert_get_response_code_is(logout_path, code=412).json()
        self.assertFalse(response["logout"])

        bad_login_without_recaptcha = {
            "email": self.user.email,
        }

        bad_login_data_incomplete = {
            "email": self.user.email,
            "google_response": "GOOGLE_RESPONSE",
        }

        bad_login_data_invalid_password = {
            "email": self.user.email,
            "password": "some_other_fake_password",
            "google_response": "GOOGLE_RESPONSE",
        }

        good_login_data = {
            "email": self.user.email,
            "password": self.user.email,
            "google_response": "GOOGLE_RESPONSE",
        }

        self.assert_post_response_code_is(login_path, bad_login_without_recaptcha, code=403)

        response = self.assert_post_response_code_is(login_path, bad_login_data_incomplete, code=400).json()
        self.assertFalse(response["login"])

        response = self.assert_post_response_code_is(login_path, bad_login_data_invalid_password, code=404).json()
        self.assertFalse(response["login"])

        response = self.assert_post_response_code_is(login_path, good_login_data, code=200).json()
        self.assertTrue(response["login"])
        self.assertEqual(response["user"]["username"], good_login_data["email"])
        self.assertEqual(response["user"]["email"], good_login_data["email"])

        response = self.assert_get_response_code_is(logout_path, code=200).json()
        self.assertTrue(response["logout"])
