from functools import reduce
from django.contrib.auth.models import User
from django.test import TestCase
from trabibi.models import TrackingObject, TrackingDetails, TrackingUpdate, TrackingGraph
from trabibi.tests.utils import get_or_create_test_user


class TestTrackingGraph(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.user = get_or_create_test_user("TestTrackingGraph")

        cls.tracking_object_names = ["T1", "T2", "T3"]
        cls.values = [100, 50, 10]
        cls.tracking_updates = []

        cls.tracking_objects = list(map(
            lambda n: TrackingObject.objects.create(name=n, user=cls.user),
            cls.tracking_object_names
        ))

        for i in range(3):
            cls.tracking_updates.append(TrackingUpdate.objects.create(
                obj=cls.tracking_objects[i],
                value=cls.values[i],
            ))

        cls.tracking_graph = TrackingGraph.objects.create(
            name="TrackingGraph",
            parent=cls.tracking_objects[0],
            operation="SUM",
        )

        cls.tracking_graph.tracked.add(*cls.tracking_objects)

        cls.tracking_details = TrackingDetails.objects.create(
            obj=cls.tracking_objects[0],
            name="Some Details",
            value="Some Value",
        )

    @classmethod
    def tearDownClass(cls):
        pass


    def test_operation_sum(self):
        """Sums all the values"""
        values_sum = reduce(lambda x, y: x + y, self.values)
        self.tracking_graph.operation = "SUM"
        self.assertEqual(self.tracking_graph.operate(), values_sum)

    def test_operation_unknown(self):
        """Returns 0 as we don't have a specified behaviour"""
        self.tracking_graph.operation = "HELLO WORLD"
        self.assertEqual(self.tracking_graph.operate(), 0)

    def test_to_frontend_schema(self):
        """Tests that the schema returned is compatible with the model"""
        schema = TrackingUpdate.to_frontend_schema()
        self.assertEqual(str, type(schema))

        splits = schema.split(",")
        self.assertTrue(len(splits) > 0)
        self.assertTrue("++id" in splits)

        for split in schema.split(","):
            if split != "++id":
                attr = getattr(TrackingUpdate, split, None)
                self.assertTrue(attr is not None)


    def test_generic(self):
        """Generic tests for the rest of the methods"""
        self.assertEqual(str, type(self.tracking_objects[0].__str__()))
        self.assertEqual(str, type(self.tracking_updates[0].__str__()))
        self.assertEqual(str, type(self.tracking_graph.__str__()))
        self.assertEqual(str, type(self.tracking_details.__str__()))

