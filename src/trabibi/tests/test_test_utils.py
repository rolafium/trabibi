from django.test import TestCase
from trabibi.tests.utils import get_or_create_test_user, get_or_create_tracking_object


class TestTestUtils(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.identifier = "TestTestUtils"
        cls.identifier2 = "TestTestUtils2"

    @classmethod
    def tearDownClass(cls):
        pass

    def _test_creations(self, func):
        """Test unique creations
        :param func: the function to be tested
        """
        obj_id = func(self.identifier).id
        # Created once
        for i in range(10):
            range_obj = func(self.identifier)
            self.assertEqual(obj_id, range_obj.id)

        # Create another one
        obj_id2 = func(self.identifier2).id
        self.assertFalse(obj_id == obj_id2)


    def test_get_or_create_test_user(self):
        """The user is created once with the correct data"""
        self._test_creations(get_or_create_test_user)

        # Correct data
        user = get_or_create_test_user(self.identifier)
        self.assertEqual(user.username, self.identifier)
        self.assertEqual(user.email, self.identifier)


    def test_get_or_create_tracking_object(self):
        """The Object is created once with the correct data"""
        self._test_creations(get_or_create_tracking_object)

        # Correct data
        tobject = get_or_create_tracking_object(self.identifier)
        self.assertEqual(tobject.name, self.identifier)
        self.assertEqual(tobject.user.username, self.identifier)
