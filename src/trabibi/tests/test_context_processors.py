from django.conf import settings
from django.test import TestCase
from django.test.client import RequestFactory
from trabibi.context_processors import global_application_state



class TestTrabibiUtils(TestCase):

    def test_global_application_state(self):
        """The dict entry should match what we expect"""
        rf = RequestFactory()
        request = rf.get("/")
        context_dict = global_application_state(request)

        self.assertEqual(settings.VERSION, context_dict["TRABIBI_VERSION"])
        self.assertEqual(settings.BUILD_NUMBER, context_dict["TRABIBI_BUILD_NUMBER"])
        self.assertEqual("{}.{}".format(settings.VERSION, settings.BUILD_NUMBER), context_dict["TRABIBI_FULL_VERSION"])
