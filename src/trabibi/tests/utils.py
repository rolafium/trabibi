from django.contrib.auth.models import User
from trabibi.models import TrackingObject


def get_or_create_test_user(identifier):
    """
    Gets or create a user based on the provided identifier
    :param identifier: str
    :returns: django.contrib.auth.models.User
    """
    user, created = User.objects.get_or_create(username=identifier, email=identifier)
    if created:
        user.set_password(identifier)
        user.save()
    return user

def get_or_create_tracking_object(identifier):
    """
    Gets or create a user based on the provided identifier
    :param identifier: str
    :returns: django.contrib.auth.models.User
    """
    user = get_or_create_test_user(identifier)
    tobject, created = TrackingObject.objects.get_or_create(name=identifier, user=user)
    return tobject
