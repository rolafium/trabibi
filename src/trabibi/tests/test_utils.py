from django.test import TestCase
from trabibi.utils import generate_model_dexie_schema


class MockField:
    def __init__(self, name):
        self.name = name

LIST_OF_FIELDS = [
    MockField("id"),
    MockField("name"),
    MockField("time"),
    MockField("hello"),
    MockField("world"),
]

class MockModel:
    class _meta:
        def get_fields():
            global LIST_OF_FIELDS
            return LIST_OF_FIELDS


class TestTrabibiUtils(TestCase):

    def test_generate_model_dexie_schema(self):
        """Should produce a plain list of fields' names"""
        global LIST_OF_FIELDS

        mapped_list = list(map(lambda item: item.name, LIST_OF_FIELDS))
        mapped_list[0] = "++id"

        self.assertEqual(mapped_list, generate_model_dexie_schema(MockModel))
