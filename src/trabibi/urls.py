from django.conf import settings
from django.conf.urls import url, include
from rest_framework import routers
from trabibi.views import (
    UserViewset,
    TrackingObjectViewset, 
    TrackingDetailsViewset, 
    TrackingUpdateViewset, 
    TrackingGraphViewset, 
    redirect_to_api,
    landing,
)

router = routers.DefaultRouter()
router.register(r'tracking-objects', TrackingObjectViewset, base_name=TrackingObjectViewset.base_name)
router.register(r'tracking-details', TrackingDetailsViewset, base_name=TrackingDetailsViewset.base_name)
router.register(r'tracking-updates', TrackingUpdateViewset, base_name=TrackingUpdateViewset.base_name)
router.register(r'tracking-graphs', TrackingGraphViewset, base_name=TrackingGraphViewset.base_name)
router.register(r'users', UserViewset, base_name=UserViewset.base_name)


urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api', redirect_to_api, name="redirect_to_api"),
    url(r'', landing, name="landing"),
]

if settings.ENVIROMENT == "DEVELOPMENT":
    import os
    from django.conf.urls.static import static
    public_folder = os.path.join(settings.PROJECT_DIR, "public")
    urlpatterns = static("/public", document_root=public_folder) + urlpatterns
