

def generate_model_dexie_schema(model):
    """
    Generates the dexie (IndexedDB) schema for the provided model
    :param model: Django Model
    :returns: List[str]
    """
    fields = [ f.name for f in model._meta.get_fields() ]
    fields[0] = "++id"
    return fields
