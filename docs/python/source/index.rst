.. Trabibi documentation master file, created by
   sphinx-quickstart on Fri Jul 13 17:46:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Trabibi's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Data Management App (data)
==========================
.. automodule:: data.management.commands.set_default_data
    :members:
.. automodule:: data.management.commands_utils
    :members:
.. automodule:: data.default_data
    :members:

Trabibi Main App
================
.. automodule:: trabibi.models
    :members:
.. automodule:: trabibi.serializers
    :members:
.. automodule:: trabibi.utils
    :members:
.. automodule:: trabibi.views
    :members:
.. automodule:: trabibi.management.commands.buildstatics
    :members:
.. automodule:: trabibi.management.commands.watchstatics
    :members:


Trabibi Configuration
=====================

.. automodule:: trabibiconf.settings
    :members:
